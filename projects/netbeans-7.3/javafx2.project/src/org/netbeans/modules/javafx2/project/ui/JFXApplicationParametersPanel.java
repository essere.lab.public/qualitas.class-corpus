/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2011 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2011 Sun Microsystems, Inc.
 */

/*
 * JFXApplicationParametersPanel.java
 *
 * Created on 22.8.2011, 13:53:57
 */
package org.netbeans.modules.javafx2.project.ui;

import org.netbeans.modules.javafx2.project.JFXProjectProperties.PropertiesTableModel;

/**
 *
 * @author Petr Somol
 */
public class JFXApplicationParametersPanel extends javax.swing.JPanel {

    private PropertiesTableModel tableModel;
    
    /** Creates new form JFXApplicationParametersPanel */
    public JFXApplicationParametersPanel(PropertiesTableModel mdl) {
        this.tableModel = mdl;
        initComponents();
        tableParams.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        labelParams = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableParams = new javax.swing.JTable();
        buttonAdd = new javax.swing.JButton();
        buttonRemove = new javax.swing.JButton();
        labelRemark = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(400, 250));
        setLayout(new java.awt.GridBagLayout());

        labelParams.setLabelFor(tableParams);
        org.openide.awt.Mnemonics.setLocalizedText(labelParams, org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "LBL_JFXApplicationParametersPanel.labelParams.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 5, 0);
        add(labelParams, gridBagConstraints);
        labelParams.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "AN_JFXApplicationParametersPanel.labelParams.text")); // NOI18N
        labelParams.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "AD_JFXApplicationParametersPanel.labelParams.text")); // NOI18N

        tableParams.setModel(tableModel);
        jScrollPane1.setViewportView(tableParams);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        add(jScrollPane1, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(buttonAdd, org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "LBL_JFXApplicationParametersPanel.buttonAdd.text")); // NOI18N
        buttonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 10);
        add(buttonAdd, gridBagConstraints);
        buttonAdd.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "AN_JFXApplicationParametersPanel.buttonAdd.text")); // NOI18N
        buttonAdd.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "AD_JFXApplicationParametersPanel.buttonAdd.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(buttonRemove, org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "LBL_JFXApplicationParametersPanel.buttonRemove.text")); // NOI18N
        buttonRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRemoveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 10);
        add(buttonRemove, gridBagConstraints);
        buttonRemove.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "AN_JFXApplicationParametersPanel.buttonRemove.text")); // NOI18N
        buttonRemove.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "AD_JFXApplicationParametersPanel.buttonRemove.text")); // NOI18N

        labelRemark.setText(org.openide.util.NbBundle.getMessage(JFXApplicationParametersPanel.class, "JFXApplicationParametersPanel.labelRemark.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(labelRemark, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

private void buttonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddActionPerformed
    tableModel.addRow();
    tableModel.fireTableDataChanged();
}//GEN-LAST:event_buttonAddActionPerformed

private void buttonRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRemoveActionPerformed
    int selIndex = tableParams.getSelectedRow();
    if (selIndex != -1) {
        tableModel.removeRow(selIndex);
        tableModel.fireTableDataChanged();
    }
}//GEN-LAST:event_buttonRemoveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAdd;
    private javax.swing.JButton buttonRemove;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelParams;
    private javax.swing.JLabel labelRemark;
    private javax.swing.JTable tableParams;
    // End of variables declaration//GEN-END:variables
}
