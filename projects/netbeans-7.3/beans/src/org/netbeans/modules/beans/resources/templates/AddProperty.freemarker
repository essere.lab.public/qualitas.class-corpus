<#if ! (propertyChangeSupport??)>
    <#assign propertyChangeSupport="propertyChangeSupport">
</#if>
<#if ! (vetoableChangeSupport??)>
    <#assign vetoableChangeSupport="vetoableChangeSupport">
</#if>

    ${access}<#if static >static </#if><#if final >final </#if>${type}<#if indexed >[]</#if> ${name}<#if final > = ${initializer}<#else><#if initializer != "" > = ${initializer}</#if></#if>;
<#if bound >

    public static final String ${PROP_NAME} = "${name}";
</#if>
<#if generateGetter >

<#if generateJavadoc >
    /**
     * Get the value of ${name}
     *
     * @return the value of ${name}
     */
</#if>
    public <#if static >static </#if>${type}<#if indexed >[]</#if> <#if type = "boolean" >is<#else>get</#if>${capitalizedName}() {
        return ${name};
    }
</#if>
<#if generateSetter >
<#if !final >

<#if generateJavadoc >
    /**
     * Set the value of ${name}
     *
     * @param ${name} new value of ${name}
<#if vetoable>
     * @throws java.beans.PropertyVetoException
</#if>
     */
</#if>
    public <#if static >static </#if>void set${capitalizedName}(${type}<#if indexed >[]</#if> ${name})<#if vetoable> throws java.beans.PropertyVetoException</#if> {
<#if bound >
        ${type}<#if indexed >[]</#if> old${capitalizedName} = this.${name};
<#if vetoable> 
        ${vetoableChangeSupport}.fireVetoableChange(${PROP_NAME}, old${capitalizedName}, ${name});
</#if>
</#if>
        <#if static >${className}.<#else>this.</#if>${name} = ${name};
<#if bound >
        ${propertyChangeSupport}.firePropertyChange(${PROP_NAME}, old${capitalizedName}, ${name});
</#if>
    }
</#if>
</#if>
<#if indexed >
<#if generateGetter >

<#if generateJavadoc >
    /**
     * Get the value of ${name} at specified index
     *
     * @param index
     * @return the value of ${name} at specified index
     */
</#if>
    public <#if static >static </#if>${type} <#if type = "boolean" >is<#else>get</#if>${capitalizedName}(int index) {
        return <#if !static >this.</#if>${name}[index];
    }
</#if>
<#if generateSetter >
<#if generateJavadoc >
    /**
     * Set the value of ${name} at specified index.
     *
     * @param index
     * @param new${capitalizedName} new value of ${name} at specified index
<#if vetoable>
     * @throws java.beans.PropertyVetoException
</#if>
     */
</#if>
    public <#if static >static </#if>void set${capitalizedName}(int index, ${type} new${capitalizedName})<#if vetoable> throws java.beans.PropertyVetoException</#if> {
<#if bound >
        ${type} old${capitalizedName} = this.${name}[index];
<#if vetoable> 
        ${vetoableChangeSupport}.fireVetoableChange(${PROP_NAME}, old${capitalizedName}, new${capitalizedName});
</#if>
</#if>
        <#if !static >this.</#if>${name}[index] = new${capitalizedName};
<#if bound >
        ${propertyChangeSupport}.fireIndexedPropertyChange(${PROP_NAME}, index, old${capitalizedName}, new${capitalizedName});
</#if>
    }
</#if>
</#if>
<#if generatePropertyChangeSupport >

    private transient final java.beans.PropertyChangeSupport ${propertyChangeSupport} = new java.beans.PropertyChangeSupport(this);

<#if generateJavadoc >
    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
</#if>
    public void addPropertyChangeListener(java.beans.PropertyChangeListener listener )
    {
        ${propertyChangeSupport}.addPropertyChangeListener( listener );
    }

<#if generateJavadoc >
    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
</#if>
    public void removePropertyChangeListener(java.beans.PropertyChangeListener listener )
    {
        ${propertyChangeSupport}.removePropertyChangeListener( listener );
    }
</#if>
<#if generateVetoablePropertyChangeSupport >

    private transient final java.beans.VetoableChangeSupport ${vetoableChangeSupport} = new java.beans.VetoableChangeSupport(this);

<#if generateJavadoc >
    /**
     * Add VetoableChangeListener.
     *
     * @param listener
     */
</#if>
    public void addVetoableChangeListener(java.beans.VetoableChangeListener listener )
    {
        ${vetoableChangeSupport}.addVetoableChangeListener( listener );
    }

<#if generateJavadoc >
    /**
     * Remove VetoableChangeListener.
     *
     * @param listener
     */
</#if>
    public void removeVetoableChangeListener(java.beans.VetoableChangeListener listener )
    {
        ${vetoableChangeSupport}.removeVetoableChangeListener( listener );
    }
</#if>