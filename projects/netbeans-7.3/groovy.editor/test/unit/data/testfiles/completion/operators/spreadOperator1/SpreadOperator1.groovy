class SpreadOperator1 {

    def testNb1() {
        ['cat', 'elephant']*.^
    }

    def testNb2() {
        ['cat', 'elephant']*.s^
    }
}
