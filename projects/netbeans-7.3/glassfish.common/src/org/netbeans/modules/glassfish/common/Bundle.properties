#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
#
# Copyright 1997-2011 Oracle and/or its affiliates. All rights reserved.
#
# Oracle and Java are registered trademarks of Oracle and/or its affiliates.
# Other names may be trademarks of their respective owners.
#
# The contents of this file are subject to the terms of either the GNU
# General Public License Version 2 only ("GPL") or the Common
# Development and Distribution License("CDDL") (collectively, the
# "License"). You may not use this file except in compliance with the
# License. You can obtain a copy of the License at
# http://www.netbeans.org/cddl-gplv2.html
# or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
# specific language governing permissions and limitations under the
# License.  When distributing the software, include this License Header
# Notice in each file and include the License file at
# nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
# particular file as subject to the "Classpath" exception as provided
# by Oracle in the GPL Version 2 section of the License file that
# accompanied this code. If applicable, add the following below the
# License Header, with the fields enclosed by brackets [] replaced by
# your own identifying information:
# "Portions Copyrighted [year] [name of copyright owner]"
#
# Contributor(s):
#
# The Original Software is NetBeans. The Initial Developer of the Original
# Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
# Microsystems, Inc. All Rights Reserved.
#
# If you wish your version of this file to be governed by only the CDDL
# or only the GPL Version 2, indicate your decision by adding
# "[Contributor] elects to include this software in this distribution
# under the [CDDL or GPL Version 2] license." If you do not indicate a
# single choice of license, a recipient has the option to distribute
# your version of this file under either the CDDL, the GPL Version 2 or
# to extend the choice of license to its licensees as provided above.
# However, if you add GPL Version 2 code and therefore, elected the GPL
# Version 2 license, then the option applies only if the new code is
# made subject to such option by the copyright holder.
#

OpenIDE-Module-Display-Category=Libraries

OpenIDE-Module-Long-Description=Common support and administration module \
for servers based on the GlassFish Server 3 codebase.

OpenIDE-Module-Name=GlassFish Server 3 - Common

OpenIDE-Module-Short-Description=Shared support module for integrating servers \
based on the GlassFish Server 3 codebase.

# Error messages
MSG_NullServerFolder=Attempt to save a server instance with null home folder

MSG_START_SERVER_FAILED_NOHOST={0} Server can't start. Host name is blank.
MSG_START_SERVER_FAILED_BADPORT={0} Server can't start.  Port is invalid.
MSG_START_SERVER_OCCUPIED_PORT={0} Server cannot start. Port is occupied.
MSG_START_SERVER_FAILED_FNF={0} Server can't start. The start jar was not found.

MSG_START_SERVER_FAILED_DOMAIN_FNF=Server can not start. The domain.xml file \
is missing or corrupt.

MSG_START_SERVER_FAILED_PD={0} Server start failed. Can't create start proccess.

MSG_START_SERVER_FAILED_JDK_ERROR=<html>Unable to start the {0} server. \
GlassFish Server 3 requires the JDK 6 platform. <br>To run GlassFish Server 3, \
you need to specify the location of the JDK 6 executable in the Servers \
manager. </html>

MSG_START_SERVER_NOT_INITIALIZED= {0} Server is not initialized.
MSG_START_SERVER_IN_PROGRESS=Starting {0}
MSG_SERVER_STARTED={0} is running.
MSG_START_SERVER_FAILED={0} Start Failed

MSG_START_SERVER_FAILED2=Could not connect to admin listener for {0}.\n\
Verify that NetBeans can make outbound connections to {1}:{2}

MSG_START_SERVER_DASDOWN={0} can't start because the Domain Admin Server \
is down.

MSG_START_TARGET_FAILED={0} did not start. {1} would not start.

MSG_STOP_SERVER_FAILED_FNF={0} Server cannot stop.
MSG_STOP_SERVER_FAILED_PD={0} Server stop failed. Can't create stop proccess.
MSG_STOP_SERVER_IN_PROGRESS=Server {0} is stopping
MSG_SERVER_STOPPED={0} was stopped.
MSG_SERVER_PROFILING_STOPPED={0} Profiling stopped.
MSG_STOP_SERVER_FAILED={0} Stop Failed
MSG_STOP_TARGET_FAILED={0} stop failed. Cannot stop {1}.

MSG_RESTART_SERVER_IN_PROGRESS=Restarting {0}
MSG_RESTART_SERVER_FAILED_WONT_START={0} Server failed to start.
MSG_RESTART_SERVER_FAILED_WONT_STOP={0} Server failed to stop.
MSG_RESTART_SERVER_FAILED_REASON_UNKNOWN={0} Server failed to start \
for unknown reason.

MSG_SERVER_RESTARTED={0} has been restarted.

MSG_ServerCmdException={0} failed on {1} : {2}
MSG_ServerCmdRunning={0} running on {1}
MSG_ServerCmdCompleted={0} completed on {1}
MSG_ServerCmdFailed={0} failed on {1} \n {2}

MSG_ServerCmdFailedIncorrectInstance={0} failed. Instance is not {1} \
or it is not running.

MSG_AuthorizationFailed=Authorization failed for {0} on {1}.  Bad password?

MSG_AuthorizationFailedRemote=Authorization failed for {0} on {1}. \
Have you run the enable-secure-admin command?

MSG_Exception={0}

# SimpleIO Output window cancel action
CTL_Cancel=Cancel
LBL_CancelDesc=Terminate this job.

MSG_QueryCancel=This will forcibly kill this process. \
Are you sure you want to do this?

DEFAULT_PRELUDE_DOMAIN_NAME=GlassFish v3 Prelude
PERSONAL_PRELUDE_DOMAIN_NAME=Personal GlassFish v3 Prelude
LBL_Creating_personal_domain=Creating personal domain
LBL_outputtab=Output
LBL_RunningCreateDomainCommand=Running the create-domain subcommand
ERR_Failed_cleanup=Failed to cleanup cancelled domain creation

MSG_delete_password_file=Failed to delete temporary password file: {0}

# This message is used to tell a user that the server process has been started
# in a particular Locale and should remain as technical as possible.
MSG_LocaleSwitched=<html>Server cannot operate in current Locale. Locale \
switched to en_US for the processs.</html>

STR_V3_SERVER_NAME=GlassFish Server 3
STR_V31_SERVER_NAME=GlassFish Server 3.1
STR_V3_AUTO_REGISTERED_NAME=GlassFish Server 3 Domain
STR_V3_AUTO_CREATED_NAME=Personal GlassFish Server 3 Domain
STR_PRELUDE_SERVER_NAME=GlassFish v3 Prelude
STR_PRELUDE_AUTO_REGISTERED_NAME=GlassFish v3 Prelude Domain
STR_PRELUDE_AUTO_CREATED_NAME=Personal GlassFish v3 Prelude Domain
MSG_FAILED_TO_UPDATE=Failed to update: {0}
nothingToList=Nothing to list.

MSG_START_SERVER_FAILED_INVALIDPORT=<html>{0} Server cannot start in \
debug mode on port {1}</html>

MSG_PASS_THROUGH={0}
MSG_INVALID_JAVA=<html>{0} Server cannot be started with {1}</html>

MSG_see_successful_results=<html>Domain creation successful.<br/><br/>View \
the results?</html>

MSG_see_failure_results=<html>Domain creation failed.<br/><br/>Do you want \
to see the detail?</html>

CTL_PasswordProtected=Password Protected
ACSD_UserNameField=Username:
ACSD_PasswordField=Password:
LAB_AUTH_User_Name=&Username:
LAB_AUTH_Password=&Password:
ACSD_NbAuthenticatorPasswordPanel=Password panel
PROMPT_GLASSFISH_AUTH=Admin Credentials for GlassFish Server @ {0}:{1}

MSG_NOOP=NoOp

WARN_UNREADABLE_LOG_STREAM=Failed to connect to log for instance {0}

MSG_SELECTED_PORT=<html>Use port {0} to attach the debugger to the \
GlassFish instance</html>

TITLE_MESSAGE=Port Selection Notice

# ServerDetails.java
STR_3_SERVER_NAME=GlassFish Server 3.0
STR_301_SERVER_NAME=GlassFish Server 3.0.1
STR_31_SERVER_NAME=GlassFish Server 3.1
STR_311_SERVER_NAME=GlassFish Server 3.1.1
STR_312_SERVER_NAME=GlassFish Server 3.1.2
STR_3122_SERVER_NAME=GlassFish Server 3.1.2.2
STR_40_SERVER_NAME=GlassFish Server 4.0

# StartTask.java
MSG_DOMAIN_UPGRADE_FAILED=<html>Automated domain upgrade failed.<br/><br/>\
Use <strong>asadmin start-domain --upgrade</strong> to complete the required \
upgrade.</html>

# CommonServerSupport.java
MSG_FLAKEY_NETWORK=<html>Network communication problem<br/>Could not establish \
a reliable connection with<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \
{0}:{1}<br/>\Error message:<br/>&nbsp;&nbsp;{2}

MSG_FLAKEY_NETWORK2=<html>Network communication problem<br/>Could not \
establish a reliable connection with<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
{3} ({0}:{1})<br/>Error message:<br/>&nbsp;&nbsp;{2}

MSG_COMMAND_SSL_ERROR=Administration command {0} timed out on {1} while \
port {2} is alive. Have you run the enable-secure-admin command?

MSG_EmptyMessage=
MSG_ADMIN_FAILED={0} admin command {1} failed
MSG_ADMIN_EXCEPTION={0} admin command {1} failed: {2}
MSG_ADMIN_LOCAL_AUTH_FAILED=Local {0} admin command {1} authorization failed
MSG_ADMIN_REMOTE_AUTH_FAILED=Remote {0} admin command {1} authorization failed
