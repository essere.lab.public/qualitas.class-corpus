<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!--       
*     Copyright © 1997, 2011, 2012, Oracle and/or its affiliates. All rights reserved.
*     Use is subject to license terms.
-->
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Attaching to a Remote Application</title>
        <link rel="StyleSheet" href="nbdocs://org.netbeans.modules.usersguide/org/netbeans/modules/usersguide/ide.css" type="text/css">
    </head>
    <body>
        <h2>Attaching to a Remote Application</h2>
        <p><small><a href="#seealso">See Also</a></small></p> 
                <p>You can profile an application that is running on a remote system 
                by attaching the profiling tool to the application.
                When you use this attach mode, the remote application starts after the profiler is attached. 
                This mode enables you to obtain profiling data on the startup of the target JVM.</p>
                
                <p>To attach the profiling tool you use the Attach Settings dialog box to specify the attachment settings for your project.
                In the Attach Settings dialog box you specify the location, OS and JVM of the remote target.
                Based on the details that you provide, the Attach Settings dialog box provides you with a set of instructions on how to 
                configure the remote application to support profiling.</p>
                
                <p>To attach the profiler to a remote application you need to configure the application to load
                some profiler libraries on startup to enable the profiling tool to attach to the application.
                You use the Attach Settings dialog box to specify the details of the remote system and to 
                generate a Remote Profiler Pack archive that contains the necessary profiler libraries.
                You need to copy the contents of the Remote Profiler Pack archive to the remote system and then configure
                the remote application to load the profiler libraries.</p>
                
                <p>After you configure the remote application according to the instructions, you can start the remote application and
                    attach the profiler.
                You only need to configure the attach mode once. The attachment settings are associated with that project. 
                You can open the Attach Settings dialog box at any time to change the attachment settings.</p> 
                
                <p><b>Perform the following steps to profile a remote application.</b></p>
                <ol>
                    <li>Choose Profile &gt; Attach Profiler from the main menu.</li>
                    <li>Select the Project from the dropdown list at the top of the Attach Profiler dialog box, if available.</li>
                    <li>At the bottom of the dialog box, click <b>define</b> to open the Attach Settings dialog box.</li>
                    <li>In the Attach Settings dialog box, select Remote as the Target option.</li>
                    <li>Specify the Hostname and select the OS and JVM from the dropdown list.</li>
                    <li>Save the Remote Profiler Pack to your local system and then copy the contents to the remote system.</li>
                    <li>Copy the profiler agent parameters in the text box of the Attach Settings dialog box and add
                        the parameters to the startup script of the remote application.</li>
                    <li>Click OK in the Attach Settings dialog box.</li>
                    <li>Start the remote application.
                        <p>When you start the remote application, the application will wait until the IDE is attached before starting.</p></li>
                    <li>Select a profiling task and any profiling options.</li>
                    <li>Click Attach in the Attach Profiler dialog box.</li>
                </ol>
            
            <p>When you click Attach, the IDE will attach to the remote JVM and the remote application will start.
            You can view profiling data as you would for a local application.</p>
            
            <p>After the IDE is attached to the remote application you can do the following: </p>
            <ul>
                <li>Detach from the remote application.
                <p>When you detach from the remote application, the remote application does not stop 
                but you stop receiving profiling data about the remote application.
                To attach to the remote application you need to use the startup options provided by the Attach Settings dialog box and 
                start the remote application again.</p></li>
                <li>Modify the profiling session.
                <p>You can modify the profiling session without detaching from the remote application.
                For example, you can change the profiling task to monitoring to reduce the profiling overhead, and then
                modify the task again later. This way you do not need to re-attach and restart the remote application.</p></li>
            </ul>
            
                <h3>Attaching to a Remote Server</h3>
                <p>Similar to attaching the profiler to a remote application, 
                you need to copy the profiler libraries in the Remote Profiler Pack to the remote system
                to attach the profiling tool to a remote server.
                You also need to modify the server configuration files to specify the path to the JDK and
                to specify the path to the profiler agent.
                When you start the server using the modified startup script, the server will wait until the profiler
                attaches to the server.
                </p>
                
                <p>You can retrieve the path to the profiler agent when you use the Attach Settings dialog box to configure your attach settings.
                The path to the profiler agent will be similar to the following path.<br>
                <tt>-agentpath:<em>&lt;remote&gt;</em>\lib\deployed\jdk16\windows\profilerinterface.dll=<em>&lt;remote&gt;</em>\lib,5140</tt><br>
                The placeholder <tt><em>&lt;remote&gt;</em></tt> refers to the full path to the root directory containing the 
                profiler libraries that you copied to the remote system.
                The number <tt>5140</tt> is the Communication Port that the profiling tool uses to connect to the application.
                You can modify the port number in the Profiler tab in the Java category in the Options window. </p>
                

                <p>The following table identifies the startup scripts and the parameters that need to be modified
                    to specify the paths to the JDK and the profiler libraries. </p>
                
                <table border="1" cellpadding="3" cellspacing="0">
                    <tr align="left" valign="top">
                        <th scope="col">Server</th>
                        <th scope="col">File</th>
                        <th scope="col">Modification</th>
                    </tr>
                    <tr>
                        <td>Tomcat 7</td>
                        <td><tt>catalina.bat/catalina.sh</tt></td>
                        <td>set <tt>JAVA_HOME</tt> to path to JDK for server<br>
                            modify <tt>CATALINA_OPTS</tt> to include profiler <tt>-agentpath</tt> parameter</td>
                    </tr>
                    <tr>
                        <td>GlassFish 3</td>
                        <td><tt>asenv.bat/asenv.conf</tt><br>
                            <tt>domain.xml</tt></td>
                        <td>set <tt>AS_JAVA</tt> to path to JDK for server<br>
                        add <tt>&lt;jvm-options&gt;</tt> elements to include profiler <tt>-agentpath</tt> parameter</td>
                    </tr>
                    <tr>
                        <td>WebLogic 12</td>
                        <td><tt>startWebLogic.cmd/startWebLogic.sh</tt></td>
                        <td>set <tt>JAVA_HOME</tt> to path to JDK for server<br>
                            modify <tt>JAVA_OPTIONS</tt> to include profiler <tt>-agentpath</tt> parameter</td>
                    </tr>
                    <tr>
                        <td>JBoss 7</td>
                        <td><tt>standalone.conf.bat/standalone.conf</tt></td>
                        <td>set <tt>JAVA_HOME</tt> to path to JDK for server<br>
                        modify <tt>JAVA_OPTS</tt> to include profiler <tt>-agentpath</tt> parameter</td>
                    </tr>
                </table>
                
                <p><strong>Notes.</strong></p>
                <ul class="note">
                    <li>For the GlassFish server you need to set the path to the JDK in the <tt>asenv.bat/asenv.conf</tt> file and
                    the path to the profiler agent in <tt>domain.xml</tt>.</li>
                    <li>For more details about configuring the servers for attaching the profiler tool,
                    see the following NetBeans FAQ:<br>
                    <object classid="java:org.netbeans.modules.javahelp.BrowserDisplayer">
                        <param name="content" value="http://wiki.netbeans.org/wiki/view/FaqProfilerAttachRemoteServer">
                        <param name="text" value="<html><u>How do I attach Profiler to a remote server?</u></html>">
                        <param name="textFontSize" value="medium">
                        <param name="textColor" value="blue">
                    </object>
                    </li>
                    <li>For more details about modifying server startup scripts, consult the documentation for the server.</li>
                </ul>
            	
        <dl>
            <dt><a name="seealso">See Also</a></dt>
            <dd><a href="profile_attach.html">Profiling Using Attach Mode</a></dd>
            <dd><a href="profile_running.html">Attaching to a Local Application</a></dd>
            <dd><a href="profile_tasks_overview.html">Selecting a Profiling Task</a></dd>
        </dl>
        <hr>
        <small><a href="../credits.html">Legal Notices</a></small>
        <table border="0" cellpadding="50">
        <tr><td>&nbsp;</td></tr>
        </table>
        
    </body>
</html>
