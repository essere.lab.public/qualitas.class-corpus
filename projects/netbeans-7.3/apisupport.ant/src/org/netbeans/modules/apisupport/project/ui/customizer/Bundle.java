package org.netbeans.modules.apisupport.project.ui.customizer;

import java.io.File;

/*QualitasCorpus.class: We have manually added this class to represent bundle files */
public class Bundle {
	
	public static String PROGRESS_loading_data() {
		
		return null;
	}
	
	public static String LBL_CustomizerTitle(String s) {
		
		return null;
	}

	public static String CTL_AddSimple() {
		
		return null;
	}
	
	public static String LBL_ProvidedTokens_T() {
		
		return null;
	}
	
	public static String ACS_ProvidedTokensTitle() {
		
		return null;
	}
	
	public static String ACS_LBL_ProvidedTokens() {
		
		return null;
	}
	
	public static String ACSD_CTL_ProvidedTokensVerticalScroll() {
		
		return null;
	}

	public static String ACS_CTL_ProvidedTokensHorizontalScroll() {
		
		return null;
	}

	public static String ACS_CTL_ProvidedTokensVerticalScroll() {
		
		return null;
	}

	public static String LBL_ProvidedTokens_NoMnem() {
		
		return null;
	}

	public static String CTL_EditModuleDependencyTitle() {
		
		return null;
	}

	public static String LBL_AddJar_DialogTitle() {
		
		return null;
	}

	public static String LBL_Corrupted_JAR(File f) {
		
		return null;
	}

	public static String LBL_Corrupted_JAR_title() {
		
		return null;
	}

	public static String MSG_PublicPackagesAddedFmt(int i) {
		
		return null;
	}

	public static String ERR_EmptyName() {
		
		return null;
	}

	public static String ERR_InvalidName() {
		
		return null;
	}

	public static String LBL_Application() {
		
		return null;
	}
	
	public static String ACSD_CTL_ProvidedTokensHorizontalScroll() {
		
		return null;
	}

}
