package org.netbeans.modules.apisupport.project.universe;

import java.io.File;

/*QualitasCorpus.class: We have manually added this class to represent bundle files */
public class Bundle {
	public static String LBL_harness_version_5_0() {
		
		return null;
	}
	
	public static String LBL_harness_version_5_0u1() {
		
		return null;
	}
	
	public static String LBL_harness_version_5_5u1() {
		
		return null;
	}
	
	public static String LBL_harness_version_6_0() {
		
		return null;
	}
	
	public static String LBL_harness_version_6_1() {
		
		return null;
	}
	
	public static String LBL_harness_version_6_5() {
		
		return null;
	}
	
	public static String LBL_harness_version_6_7() {
		
		return null;
	}
	
	public static String LBL_harness_version_6_8() {
		
		return null;
	}
	
	public static String LBL_harness_version_6_9() {
		
		return null;
	}
	
	public static String LBL_harness_version_7_0() {
		
		return null;
	}
	
	public static String LBL_harness_version_7_1() {
		
		return null;
	}
	
	public static String LBL_harness_version_7_2() {
		
		return null;
	}
	
	public static String LBL_harness_version_7_3() {
		
		return null;
	}
	
	public static String LBL_harness_version_unknown() {
		
		return null;
	}
	
	public static String junit_placeholder() {
		
		return null;
	}
	
	public static String MSG_InvalidPlatform(File f) {
		
		return null;
	}
	
	public static String LBL_default_plaf() {
		
		return null;
	}
	
	public static String MSG_scanning_layers() {
		
		return null;
	}
	
}
