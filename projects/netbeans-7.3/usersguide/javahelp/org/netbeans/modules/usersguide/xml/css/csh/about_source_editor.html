<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!--       
 * Copyright  © 1997, 2011, Oracle and/or its affiliates. All rights reserved.
 *     Use is subject to license terms.
 *
-->
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">

        <title>About the CSS Source Editor</title>
        <link href="nbdocs://org.netbeans.modules.usersguide/org/netbeans/modules/usersguide/ide.css" 
              rel="StyleSheet" type="text/css">

    </head>

    <body>
        <h2>About the CSS Source Editor</h2>
        <p> <small><a href="#seealso">See Also</a>&nbsp; </small>
        <p>The CSS Source Editor is a full-featured text editor that is integrated with 
            other parts of the IDE. You open the Source Editor by 
            creating a new CSS file or double-clicking a CSS file node in the Projects window, Files window, or 
            Navigator window. 
        </p>
        <p>The CSS editor has the following features:</p>
        <dl>
            <dd><a href="#tabs">Tabs</a></dd>
            <dd><a href="#toolbar">Toolbar</a></dd>
            <dd><a href="#margins">Left Margin</a></dd>
            <dd><a href="#errorstripe">Error Stripe</a></dd>  
            <dd><a href="#status">Status Line</a></dd>
            <dd><a href="#colors">Syntax Coloring</a></dd>
            <dd><a href="#completion">Code Completion</a></dd>
            <dd><a href="#style-builder">CSS Styles Window</a></dd>
            <dd><a href="#pair">Pair Completion</a></dd>
            <dd><a href="#insert">Insert Mode and Overwrite Mode</a></dd>
            <dd><a href="#preview">Preview Window</a></dd>

        </dl>
        <h3><a name="tabs">Source Editor Tabs</a></h3>
        <p>The top of the Source Editor has a tab for each open document. Each tab shows
            the name of the document. If the document has been modified and has not been
            saved, there is an asterisk (*) after its name. </p>
        <p>If multiple files are open, you can split the Source Editor view by clicking
            and dragging the tab. As you drag to different parts of the editing
            area, you see a red outline that shows you where the new
            window will open. When one of these boxes appears, you can drop the document
            and split the pane.</p>
        <ul class="note">
            <li>You must close the split file if you want the editor to return to its unsplit
                state. </li>
        </ul>
        <p>You can right-click a tab to access the following commands:</p>
        <ul>
            <li><b>Close All Documents.</b> Closes all files that are open in the Source 
                Editor area.</li>
            <li><b>Close Other Documents. </b> Closes all files except for the selected file.</li>
            <li><b>Save Document.</b> Saves the selected file.</li>
            <li><b>Clone Document.</b> Opens a new tab for the same document. This command is 
                particularly useful in combination with splitting the window so that you can view 
                different parts of the same file simultaneously.</li>
            <li><b>Close Window.</b> Closes the selected file.</li>
            <li><b>Maximize Window/Restore Window.</b> Maximize Window hides other windows and 
                expands the Source Editor to use all of the window space in the IDE. Restore Window 
                returns the windows to the state that they were before you chose Maximize Window. 
                You can also maximize and restore the Source Editor by double-clicking a tab.</li>
            <li><b>Undock/Dock Window</b> Opens document in an editor in a new window. If window is 
                already undocked, the Dock Window option appears instead.</li>
<li><strong>New Document Tab Group/Collapse Document Tab Group.</strong> Splits the editor window into two groups of tabs and separates the selected tab into the new group. If a separate group of tabs exists, Collapse Document Tab Group combines these groups into one group.</li>
            <li><b>Select in Projects</b> Highlights the file's node in the Projects window.</li>
            <li><b>Local History</b> Opens a menu from which you can view the changes you made to the
                file or revert those changes.</li>
            <li><b>SVN/Git/Mercurial/etc</b> If the file is under version control, this option appears. 
                The option opens a menu of relevant version control commands.</li>
            <li><b>Diff To...</b> Diff the file to another file in its folder or another file open in the editor.</li>
        </ul>
        <h3><a name="toolbar">Toolbar</a></h3>
        <p>The editor toolbar at the top of the Source Editor window has
            buttons for various navigating and editing shortcuts. 
            Hold the cursor over a button to display
            a description of the command. </p>
        <h3><a name="margins">Left Margin</a></h3>
        <p> The left margin displays annotation glyphs that indicate line status, such
            as warnings.  </p>

        <p>You can right-click the left margin to display a pop-up menu. The margin
            can also optionally display line numbers.</p>
        <h3><a name="errorstripe">Error Stripe</a></h3>
        <p>The error stripe is the strip to 
            the right of the right scroll bar and 
            contains marks for various things in your file, such as errors, bookmarks, 
            and comments for the To Do list. The error stripe represents the whole file, not 
            just the lines currently displayed. You can immediately identify whether your 
            file has any errors without having to scroll through the entire file. You can double-click 
            a mark in the error stripe to jump to the line that the mark refers to.</p>
        <h3><a name="status">Status Line</a></h3>
        <p> The Source Editor status line is in the bottom right corner of the IDE. The first
            area of the status line shows the current line number and row number in the
            form <tt>line:row</tt>. The second area of the status line indicates the insertion
            mode (INS or OVR). The text area on the right is used for status messages.</p>
        <h3><a name="colors">Syntax Coloring</a></h3>
        <p> Source code displayed in the Source Editor is syntactically colored. For example,
            all CSS keywords are shown in blue and all CSS comments in light gray.  </p>
        <h3><a name="completion">Code Completion</a></h3>
        <p>The IDE's code completion feature helps you fill in code and tags as you are typing. 
            Code completion is available for CSS, HTML, and XML files.
        </p>
        
        
        <h3><a name="pair">Pair Completion</a></h3>
        <p>When you edit CSS, the Source Editor does matching of pair characters
            such as brackets, parentheses, and quotation marks. Closing characters are
            not duplicated if you type them yourself rather than letting the editor add
            them for you. </p>
        <h3><a name="insert">Insert Mode and Overwrite Mode</a></h3>
        <p>When the Source Editor is in insert mode, the default insertion point is a vertical
            bar, and text that you type is inserted. In overwrite mode, the default insertion
            point is a solid block, and text that you type replaces the existing text. </p>
        <p>Use the Insert key to toggle between the two modes. </p>
        <p>Whenever the insertion point in the Source Editor is located immediately after
            a brace, bracket, or parenthesis, the matching brace, bracket, or parenthesis
            is highlighted. </p>
        <ul class="note">
            <li>If the Source Editor beeps when you try to enter new text, the file is a
                read-only file. </li>
        </ul>
        <h3><a name="style-builder"></a>CSS Styles Window</h3>
        <p>The IDE includes a CSS Styles window that can help you define 
            the properties and values for each HTML element.
            For each property you can select standard values from a dropdown list.
        You can use the upper pane of the CSS Styles window to quickly locate and navigate to elements in the style sheet. 
        </p>
        <p>If the CSS Styles window does not open when you open the CSS file you can open the window by choosing Window &gt; Web &gt; CSS Styles from the main menu.
            The CSS Styles window opens in the main window next to the Editor window.</p>
        <!--<p>To use the CSS Styles window, go to the Editor window and place your cursor 
            in the element whose style you want to define. A set of tabs appear in the Style
            Builder. The tabs correspond to style categories, such as Font and Background. 
        Select a tab and select style settings in that tab. For example, to set the font
        of the &lt;h2&gt; element as Verdana, find <span style="color:green"><b>h2</b></span>
        in the Editor, open the Font tab in the Style builder, and select "Verdana, Arial, Helvetica,
        sans-serif" in the list of fonts.  
        </p>
        <ul class="note">
            <li>Use the scrollbars in the Style Builder. Some of the selections may be hidden. You
            can increase the size of the Style Builder window by raising its top border.</li>
            <li>Combine the Style Builder and the <a href="#preview">Preview Window</a> to 
            experiment easily with styles.</li>
        </ul>
        
        <h3><a name="preview"></a>Preview Window</h3>
        <p>The IDE provides a Preview window in which you can preview what HTML elements
            look like according to your CSS. By default, the Preview window appears below and to 
            the right of the Editor window when you open a CSS file. If the Preview window is
            closed, open it by going to Window &gt; Other &gt; CSS Preview.</p>
        <p>The Preview window requires a CSS Previewer plugin. The NetBeans Update Center
            includes one CSS Previewer, the Flying Saucer Renderer. To install Flying Saucer,
            go to Tools &gt; Plugins, open the Available Plugins tab, and find the CSS Preview
            Flying Saucer plugin.</p>
        <p>To use the Preview window, select a CSS element in the Editor window. If the previewer
            can render the element, sample text in that style appears in the Preview window.</p>
-->
        <dl>
            <dt><a name="seealso">See Also</a></dt>
            <dd><a href="../css_create.html">Creating a Cascading Style Sheet</a></dd>
            <dd><a href="../css_edit.html">Editing a Cascading Style Sheet</a></dd>
        </dl>

        <hr>
        <small><a href="../../../credits.html">Legal Notices</a></small>
        <table cellpadding="50" border="0">
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
