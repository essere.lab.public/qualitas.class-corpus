<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!--       
*     Copyright © 1997, 2011, Oracle and/or its affiliates. All rights reserved.
*     Use is subject to license terms.
-->
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>About the HTML and JSP Source Editor</title>
<link href="nbdocs://org.netbeans.modules.usersguide/org/netbeans/modules/usersguide/ide.css" 
    rel="StyleSheet" type="text/css">

</head>

<body>
<h2>About the HTML and JSP Source Editor</h2>
<p> <small><a href="#seealso">See Also</a></small> 
<p>The HTML/JSP Source Editor is a full-featured text editor that is integrated with 
    other parts of the IDE. You open the Source Editor by 
creating a new HTML or JSP file or double-clicking an HTML or JSP file node in the Projects window, Files window, or 
Navigator window. 
</p>
<p>The HTML/JSP editor has the following features:</p>
<dl>
  <dd><a href="#tabs">Tabs</a></dd>
  <dd><a href="#toolbar">Toolbar</a></dd>
  <dd><a href="#palette">Palette</a></dd>
  <dd><a href="#margins">Left Margin</a></dd>
  <dd><a href="#errorstripe">Error Stripe</a></dd>  
  <dd><a href="#status">Status Line</a></dd>
  <dd><a href="#colors">Syntax Coloring</a></dd>
  <dd><a href="#completion">Code Completion and Text Hints</a></dd>
  <dd><a href="#surround-tags">Surround With Tags</a></dd>
  <dd><a href="#tag-rename">Renaming Tags</a></dd>
  <dd><a href="#insert">Insert Mode and Overwrite Mode</a></dd>
</dl>
<h3><a name="tabs">Source Editor Tabs</a></h3>

<p>The top of the Source Editor has a tab for each open document. Each tab shows
  the name of the document. If the document has been modified and has not been
  saved the name of the document is displayed in bold in the tab. </p>
<p>If multiple files are open, you can split the Source Editor view by clicking
 and dragging the tab. As you drag to different parts of the editing
        area, you see a red outline that shows you where the new
        window will open. When one of these boxes appears, you can drop the document
        and split the pane.</p>
<ul class="note">
  <li>You must close the split file if you want the editor to return to its unsplit
    state. </li>
</ul>
<p>You can right-click a tab to access the following commands:</p>
<ul>
  <li><b>Close All.</b> Closes all files that are open in the Source 
Editor area.</li>
<li><b>Close Other. </b> Closes all files except for the selected file.</li>
<li><b>Save.</b> Saves the selected file.</li>
<li><b>Clone.</b> Opens a new tab for the same document. This command is 
particularly useful in combination with splitting the window so that you can view 
different parts of the same file simultaneously.</li>
<li><b>Close Window.</b> Closes the selected file.</li>
<li><b>Maximize Window/Restore Window.</b> Maximize Window hides other windows and 
expands the Source Editor to use all of the window space in the IDE. Restore Window 
returns the windows to the state that they were before you chose Maximize Window. 
You can also maximize and restore the Source Editor by double-clicking a tab.</li>
<li><b>Float/Dock Window</b> Opens document in an editor in a new window. If window is 
already floating, the Dock Window option appears instead.</li>
<li><strong>New Document Tab Group/Collapse Document Tab Group.</strong> Splits the editor window into two groups of tabs and separates the selected tab into the new group. If a separate group of tabs exists, Collapse Document Tab Group combines these groups into one group.</li>
<li><b>Select in Projects</b> Highlights the file's node in the Projects window.</li>
<li><b>Local History</b> Opens a menu from which you can view the changes you made to the
file or revert those changes.</li>
<li><b>[version control]</b> If the file is under version control, a menu item with the name of the version control system appears. 
    The option opens a menu of relevant version control commands.</li>
<li><b>Diff To...</b> Diff the file to another file in its folder or another file open in the editor.</li>
</ul>
<h3><a name="toolbar">Toolbar</a></h3>
<p>The <a href="html_editor_toolbar.html">editor toolbar</a> is at the top of the Source Editor window. The toolbar has
  buttons for various navigating and editing shortcuts, which vary according
  to the type of file you are editing. Hold the cursor over a button to display
  a description of the command. </p>
<h3><a name="palette"></a>Palette</h3>
<p>Go to Window &gt; Palette or press Ctrl-Shift-8 and the Palette opens on the right side of the Source Editor. Code snippets for many of the most common HTML tags
are provided, grouped according to their function:
<ul>
	<li><b>HTML.</b> Table, Ordered List, Unordered List, Image, Link, MetaData.
	<li><b>HTML Forms.</b> Form, Multi-line Input, Checkbox, File Select, Text Input, Drop-down list,
            Radio Button, Button.</li>
        <li><b>JSP (JSP only).</b> Use Bean, Get Bean Property, Set Bean Property, JSTL Choose, JSTL If, JSTL For Each</li>
        <li><strong>JSF (JSP and XHTML only).</strong> Metadata, JSF Form, JSF Form from Entity, JSF Data Table, JSF Data Table from Entity</li>
        <li><strong>Database (JSP only).</strong> DB Query, DB Insert, DB Delete, DB Report, DB Update</li>
</ul>
<h3><a name="margins">Left Margin</a></h3>
<p> The left margin displays annotation glyphs that indicate line status, such
  as warnings.  </p>

<p>You can right-click the left margin to display a pop-up menu. The margin
  can also optionally display line numbers.</p>
<h3><a name="errorstripe">Error Stripe</a></h3>
<p>The error stripe is the strip to 
the right of the right scroll bar and 
contains marks for various things in your file, such as errors, bookmarks, 
  and comments for the To Do list. The error stripe represents the whole file, not 
  just the lines currently displayed. You can immediately identify whether your 
file has any errors without having to scroll through the entire file. You can double-click 
a mark in the error stripe to jump to the line that the mark refers to.</p>
<h3><a name="status">Status Line</a></h3>
<p> The Source Editor status line is in the bottom right corner of the IDE. The first
  area of the status line shows the current line number and row number in the
  form <tt>line:row</tt>. The second area of the status line indicates the insertion
  mode (INS or OVR). The text area on the right is used for status messages.</p>
<h3><a name="colors">Syntax Coloring</a></h3>
<p> Source code displayed in the Source Editor is syntactically colored. For example,
  all HTML keywords are shown in blue and all HTML comments in light gray.  </p>
<h3><a name="completion">Code Completion and Text Hints</a></h3>
<p>The IDE's code completion feature helps you fill in code and tags as you are typing. 
Code completion is available for PHP, HTML, JSP, and XML files. Code completion applies both to 
tag names and attributes.
  </p>
  <ul class="note">
      <li>If code completion does not appear, press Ctrl-Space.</li>
  </ul>
  
  <p>With your cursor inside text in an HTML/JSP document, press Ctrl-Space and a menu 
      of hints appears. The hints include spell checking and HTML objects (the 
      selection of objects is the same as in the <a href="#palette">palette</a>.</p>
  <img src="../../images/html-inline-hints.png" width="195" height="201" alt="html inline hints, showing spellchecking and HTML object choices"/>
  <p>Use the left and right arrows to move your cursor, and a new set of context-relevant hints 
      or code completion options appears for every cursor position.</p>
  <p>Select an item from the list of hints and the IDE inserts this item into your document. 
      After you select an item, code completion closes. Press Ctrl-Space to reopen 
      code completion.</p>
  <h3><a name="surround-tags"></a>Surround With Tags/Remove Surrounding Tag (HTML only)</h3>
  <p>You can surround text with HTML tags or remove the innermost HTML tags surrounding some text. Select text in the editor and press 
      Alt-Enter, then select Surround With Tags or Remove Surrounding Tags. If you select Surround With Tags, The selected text is surrounded 
      with tags and the cursor is in the opening tag. Type the tag you want and 
      the closing tag automatically changes to match. See also 
      <a href="#tag-rename">Renaming Tags</a>. </p>
  <h3><a name="tag-rename"></a>Renaming Tags (HTML only)</h3>
  <p>To rename a tag, place your cursor inside either the opening or closing tag and press Ctrl-R. The name of the tag is surrounded by a red box. <img src="../../images/html-rename1.png" width="161" height="19" alt="Tags after CTRL-R is pressed"/>
  </p>
  <p>Type the tag you want. Opening and closing tags change at the same time. Press Enter when you are done.</p>
<h3><a name="insert">Insert Mode and Overwrite Mode</a></h3>
<p>When the Source Editor is in insert mode, the default insertion point is a vertical
  bar, and text that you type is inserted. In overwrite mode, the default insertion
  point is a solid block, and text that you type replaces the existing text. </p>
<p>Use the Insert key to toggle between the two modes. </p>
<p>Whenever the insertion point in the Source Editor is located immediately after
  a brace, bracket, or parenthesis, the matching brace, bracket, or parenthesis
  is highlighted. </p>
<ul class="note">
  <li>If the Source Editor beeps when you try to enter new text, the file is a
    read-only file. </li>
</ul>

<dl>
<dt><a name="seealso">See Also</a></dt>
<dd><a href="../html_create.html">Creating an HTML File</a></dd>
</dl>
<hr>
<small><a href="../../credits.html">Legal Notices</a></small>
<table cellpadding="50" border="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
