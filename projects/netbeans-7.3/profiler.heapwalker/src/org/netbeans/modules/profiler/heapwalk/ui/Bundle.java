package org.netbeans.modules.profiler.heapwalk.ui;

import javax.swing.Icon;

/*QualitasCorpus.class: We have manually added this class to represent bundle files */
public class Bundle {

	public static String AnalysisControllerUI_ControllerName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String AnalysisControllerUI_ControllerDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String AnalysisControllerUI_RulesToApplyString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon AnalysisControllerUI_PerformButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String AnalysisControllerUI_InfoString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String AnalysisControllerUI_AnalysisResultsText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String AnalysisControllerUI_ProcessingRulesMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon AnalysisControllerUI_CancelButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesControllerUI_ControllerName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesControllerUI_ControllerDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_ViewTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ClassesListControllerUI_FilterCheckboxText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_ShowHideColumnsString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ClassesListControllerUI_ShowInInstancesString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object ClassesListControllerUI_ShowImplementationsString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object ClassesListControllerUI_ShowSubclassesString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ClassesListControllerUI_GoToSourceString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_ClassNameColumnText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_InstancesRelColumnText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_InstancesRelColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_InstancesColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_SizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_RetainedSizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_InstancesColumnText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_RetainedSizeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_ClassNameColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_SizeColumnText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_CompareWithAnotherText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_ComparingMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_ClassesTableAccessName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_ClassesTableAccessDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object ClassesListControllerUI_FilterImplementation() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object ClassesListControllerUI_FilterSubclass() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_DefaultFilterText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_FilteringProgressText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_NoClassInBaseMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ClassesListControllerUI_NoInstancesMsg(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_ViewTitleFields() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_ViewTitleStaticFields() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_ShowHideColumnsString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_ShowInstanceItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_ShowInInstancesItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_ShowInClassesItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_ShowClassItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon InstancesListControllerUI_CopyIdString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon FieldsBrowserControllerUI_CopyPathFromRoot() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon FieldsBrowserControllerUI_ShowLoopItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon FieldsBrowserControllerUI_GoToSourceItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_FieldColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_FieldColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_TypeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_TypeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_FullTypeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_FullTypeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_ValueColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_ValueColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_SizeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_SizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_RetainedSizeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FieldsBrowserControllerUI_RetainedSizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_NoInstanceSelectedMsg(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object FieldsBrowserControllerUI_NoClassSelectedMsg(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ReferencesBrowserControllerUI_ShowGcRootItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_ShowHideColumnsString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_InstanceColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_InstanceColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_ObjidColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_ObjidColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_SizeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_SizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_RetainedSizeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_RetainedSizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesListControllerUI_ViewCaption() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SummaryControllerUI_ViewTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SummaryControllerUI_ViewDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OverviewControllerUI_ViewTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OverviewControllerUI_InProgressMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OpenHeapWalkerAction_DialogCaption() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String FileDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String HeapFragmentWalkerUI_NavigateBackName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object HeapFragmentWalkerUI_NavigateBackDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String HeapFragmentWalkerUI_NavigateForwardName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Object HeapFragmentWalkerUI_NavigateForwardDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String HeapWalkerUI_ComponentDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesControllerUI_ViewCaption() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesControllerUI_ViewDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String InstancesControllerUI_NoClassDefinedMsg(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_ViewTitleReferences() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_ShowHideColumnsString() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ReferencesBrowserControllerUI_ShowInstanceItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ReferencesBrowserControllerUI_ShowLoopItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ReferencesBrowserControllerUI_CopyPathFromRoot() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ReferencesBrowserControllerUI_GoToSourceItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon ReferencesBrowserControllerUI_ShowInThreadsItemText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_FieldColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_FieldColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_TypeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_TypeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_FullTypeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_FullTypeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_ValueColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_ValueColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_SizeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_SizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_RetainedSizeColumnName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_RetainedSizeColumnDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ReferencesBrowserControllerUI_NoInstanceSelectedMsg(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_OkButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_SaveQueryCaption() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_UpButtonToolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_UpButtonAccessName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_DownButtonToolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_DownButtonAccessName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_CloseButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_QueryPropertiesCaption(String caption) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_NewQueryRadioText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_NameLabelText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_DefaultQueryName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_DescriptionLabelText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_ExistingQueryRadioText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLQueryCustomizer_UpdateQueryLabelText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_ControllerName() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_ControllerDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_QueryResultsCaption() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_QueryEditorCaption() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_ExecutingQueryMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_ExecuteButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_ExecuteButtonAccessDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_SaveButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_SaveButtonAccessDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_CancelButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_CancelButtonAccessDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_SavedQueriesCaption() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_LoadingQueriesMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_NoSavedQueriesMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_OpenButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_OpenButtonAccessDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_PropertiesButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_PropertiesButtonAccessDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_DeleteButtonAccessDescr() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String OQLControllerUI_DeleteButtonText() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String HintsControllerUI_ViewTitleHints() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String HintsControllerUI_Label1String() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String HintsControllerUI_Label2String() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon HintsControllerUI_FindButton() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String HintsControllerUI_FindButtonTooltip() {
		// TODO Auto-generated method stub
		return null;
	}

}
