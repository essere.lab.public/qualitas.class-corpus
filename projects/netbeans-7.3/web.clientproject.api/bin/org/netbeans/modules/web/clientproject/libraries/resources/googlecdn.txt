AngularJS
name: angularjs
latest version: 1.0.2, 1.0.1
path: https://ajax.googleapis.com/ajax/libs/angularjs/1.0.2/angular.min.js
path(u): https://ajax.googleapis.com/ajax/libs/angularjs/1.0.2/angular.js
site: http://angularjs.org/

Chrome Frame
name: chrome-frame
latest version: 1.0.3, 1.0.2, 1.0.0, 1.0.1
path: https://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js
path(u): https://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.js
site: https://code.google.com/chrome/chromeframe/

Dojo
name: dojo
latest version: 1.8.1, 1.8.0, 1.7.3, 1.7.2, 1.7.1, 1.7.0, 1.6.1, 1.1.1, 1.2.0, 1.2.3, 1.3.0, 1.3.1, 1.3.2, 1.4.0, 1.4.1, 1.4.3, 1.5.0, 1.5.1, 1.6.0
path: https://ajax.googleapis.com/ajax/libs/dojo/1.8.1/dojo/dojo.js
site: http://dojotoolkit.org/

Ext Core
name: ext-core
latest version: 3.1.0, 3.0.0
path: https://ajax.googleapis.com/ajax/libs/ext-core/3.1.0/ext-core.js
path(u): https://ajax.googleapis.com/ajax/libs/ext-core/3.1.0/ext-core-debug.js
site: http://www.sencha.com/products/extjs/

jQuery
name: jquery
latest version: 1.8.2, 1.8.1, 1.8.0, 1.7.2, 1.7.1, 1.2.3, 1.2.6, 1.3.0, 1.3.1, 1.3.2, 1.4.0, 1.4.1, 1.4.2, 1.4.3, 1.4.4, 1.5.0, 1.5.1, 1.5.2, 1.6.0, 1.6.1, 1.6.2, 1.6.3, 1.6.4, 1.7.0
path: https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
path(u): https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js
site: http://jquery.com/

jQuery UI
name: jqueryui
latest version: 1.9.1, 1.9.0, 1.8.24, 1.8.23, 1.8.22, 1.8.21, 1.8.20, 1.8.19, 1.8.18, 1.8.17, 1.8.16, 1.5.2, 1.5.3, 1.6.0, 1.7.0, 1.7.1, 1.7.2, 1.7.3, 1.8.0, 1.8.1, 1.8.2, 1.8.4, 1.8.5, 1.8.6, 1.8.7, 1.8.8, 1.8.9, 1.8.10, 1.8.11, 1.8.12, 1.8.13, 1.8.14, 1.8.15
path: https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js
path(u): https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.js
site: http://jquery.com/

MooTools
name: mootools
latest version: 1.4.5, 1.4.4, 1.4.3, 1.4.2, 1.4.1, 1.1.1, 1.1.2, 1.2.1, 1.2.2, 1.2.3, 1.2.4, 1.2.5, 1.3.0, 1.3.1, 1.3.2, 1.4.0
path: https://ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js
path(u): https://ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools.js
site: http://mootools.net/

Prototype
name: prototype
latest version: 1.7.1.0, 1.7.0.0, 1.6.0.2, 1.6.0.3, 1.6.1.0
path: https://ajax.googleapis.com/ajax/libs/prototype/1.7.1.0/prototype.js
site: http://prototypejs.org/

script.aculo.us
name: scriptaculous
latest version: 1.9.0, 1.8.1, 1.8.2, 1.8.3
path: https://ajax.googleapis.com/ajax/libs/scriptaculous/1.9.0/scriptaculous.js
site: http://script.aculo.us/

SWFObject
name: swfobject
latest version: 2.2, 2.1
path: https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js
path(u): https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject_src.js
site: http://code.google.com/p/swfobject/

WebFont Loader
name: webfont
latest version: 1.0.31, 1.0.30, 1.0.29, 1.0.28, 1.0.27, 1.0.26, 1.0.25, 1.0.24, 1.0.0, 1.0.1, 1.0.2, 1.0.3, 1.0.4, 1.0.5, 1.0.6, 1.0.9, 1.0.10, 1.0.11, 1.0.12, 1.0.13, 1.0.14, 1.0.15, 1.0.16, 1.0.17, 1.0.18, 1.0.19, 1.0.21, 1.0.22, 1.0.23
path: https://ajax.googleapis.com/ajax/libs/webfont/1.0.31/webfont.js
path(u): https://ajax.googleapis.com/ajax/libs/webfont/1.0.31/webfont_debug.js
site: http://code.google.com/apis/webfonts/docs/webfont_loader.html
