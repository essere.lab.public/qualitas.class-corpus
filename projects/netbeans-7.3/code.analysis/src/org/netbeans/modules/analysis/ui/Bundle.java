package org.netbeans.modules.analysis.ui;

import org.netbeans.modules.analysis.Configuration;

/*QualitasCorpus.class: We have manually added this class to represent bundle files */
public class Bundle {

	static public String HINT_AnalysisResultTopComponent() {
		
		return null;
	}
	
	static public String CTL_NewConfig() {
		
		return null;
	}
	
	static public String ConfigDefaultName() {
		
		return null;
	}
	
	static public String CTL_Duplicate() {
		
		return null;
	}
	
	static public String CTL_Rename() {
		
		return null;
	}
	
	static public String MSG_ReallyDeleteConfig(Configuration c) {
		
		return null;
	}
	
	
	static public String DeleteConfigTitle() {
		
		return null;
	}
	
	static public String DESC_MissingPlugins(String s) {
		
		return null;
	}
	
	static public String DN_MissingPlugins() {
		
		return null;
	}
	
	static public String LBL_InstallPlugins() {
		
		return null;
	}
	
	static public String CTL_AnalysisResultTopComponent() {
		
		return null;
	}
	
	static public String CTL_Delete() {
		
		return null;
	}


}
