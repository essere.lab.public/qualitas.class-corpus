package org.netbeans.modules.maven.codegen;

/*QualitasCorpus.class: We have manually added this class to represent bundle files */
public class Bundle {
	
	static public String MSG_Cannot_Parse() {
		
		return null;
	}

	static public String ERR_CannotWriteModel(String s) {
		
		return null;
	}

	static public String NAME_Dependency() {
		
		return null;
	}

	static public String NAME_Exclusion() {
		
		return null;
	}

	static public String TIT_Add_License() {
		
		return null;
	}

	static public String NAME_License() {
		
		return null;
	}

	static public String NAME_Mirror() {
		
		return null;
	}

	static public String TIT_Add_mirror() {
		
		return null;
	}

	static public String LBL_Incomplete() {
		
		return null;
	}

	static public String NewProfilePanel_cbPlugins_text2() {
		
		return null;
	}

	static public String NewProfilePanel_cbDependencies_text2() {
		
		return null;
	}

	static public String NAME_Plugin() {
		
		return null;
	}

	static public String TIT_Add_plugin() {
		
		return null;
	}

	static public String NAME_Profile() {
		
		return null;
	}

	static public String TIT_Add_profile() {
		
		return null;
	}
}
