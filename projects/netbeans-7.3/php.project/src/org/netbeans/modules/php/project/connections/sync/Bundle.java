package org.netbeans.modules.php.project.connections.sync;

import javax.swing.Icon;

import org.netbeans.modules.php.project.connections.sync.SyncItem.Operation;

/*QualitasCorpus.class: We have manually added this class to represent bundle files */
public class Bundle {

	public static String SyncPanel_button_titleWithMnemonics() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_title(String name, String remoteConfigurationName) {
		// TODO Auto-generated method stub
		return null;
	}

	public static Operation SyncPanel_view_warning() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Operation SyncPanel_view_error() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_table_header_info_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_table_header_operation_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_table_header_remotePath_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_table_header_localPath_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_sort_local_asc() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_swapPaths() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_sort_remote_asc() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_sort_local_desc() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_sort_remote_desc() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_sort_info() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_popupMenu_resetItem() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_disable_download() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_disable_upload() {
		// TODO Auto-generated method stub
		return null;
	}

	public static Icon SyncPanel_popupMenu_disable_delete() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_popupMenu_diffItem() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_resetButton_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_diffButton_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_experimental() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_firstRun() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_individualFiles() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_error_operations() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_warn_operations() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_collectingInformation() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_prefix_error(String name, String message) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_prefix_warning(String name, String message) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_status(int download, int upload, int delete, int noop, int errors, int warnings) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_prefix_all(String info) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_info_prefix_selection(String info) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_error_documentSave() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_table_column_remote_title() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_table_column_local_title() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_localFile_modified_mark(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncPanel_operation_tooltip(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_noop_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_noop_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_download_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_download_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_downloadReview_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_upload_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_uploadReview_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_delete_toolTip() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_fileDirCollision_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_fileConflict_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_symlink_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_delete_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String Operation_upload_titleWithMnemonic() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_error_fileConflict() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_error_fileDirCollision() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_warn_symlink() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_error_childNotDeleted() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_error_cannotDownload() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_warn_downloadReview() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_error_cannotUpload() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncItem_warn_uploadReview() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncController_fetching_files(int length) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncController_fetching_project(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncController_error_tmpFileCopyFailed() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncController_error_deleteLocalFile() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SyncController_error_localFolderNotEmpty() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SummaryPanel_button_titleWithMnemonics() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SummaryPanel_title() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String SummaryPanel_na() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_progress_title() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_button_cancel() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_title() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_cancel() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_success() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_details_output() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_button_ok() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_error() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_uploading(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String ProgressPanel_downloading(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
