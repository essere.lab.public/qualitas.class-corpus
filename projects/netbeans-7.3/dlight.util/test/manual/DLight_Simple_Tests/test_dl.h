/* 
 * File:   test_dl.h
 * Author: vk155633
 *
 * Created on January 30, 2009, 4:05 PM
 */

#ifndef _TEST_DL_H
#define	_TEST_DL_H

#ifdef	__cplusplus
extern "C" {
#endif


void test_dl(int step);

#ifdef	__cplusplus
}
#endif

#endif	/* _TEST_DL_H */

