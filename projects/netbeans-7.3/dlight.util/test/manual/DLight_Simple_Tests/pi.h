/* 
 * File:   pi.h
 * Author: vk155633
 *
 * Created on December 17, 2008, 6:01 PM
 */

#ifndef _PI_H
#define	_PI_H

#ifdef	__cplusplus
extern "C" {
#endif

double calc_pi(int num_steps);

#ifdef	__cplusplus
}
#endif

#endif	/* _PI_H */

