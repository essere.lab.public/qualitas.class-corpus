/* 
 * File:   test_write.h
 * Author: vk155633
 *
 * Created on January 15, 2009, 5:54 PM
 */

#ifndef _TEST_WRITE_H
#define	_TEST_WRITE_H

#ifdef	__cplusplus
extern "C" {
#endif

void test_write(int mb);

#ifdef	__cplusplus
}
#endif

#endif	/* _TEST_WRITE_H */

