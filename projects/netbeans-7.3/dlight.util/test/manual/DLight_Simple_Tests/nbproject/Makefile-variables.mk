#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# GNU configuration
CND_PLATFORM_GNU=GNU-Solaris-x86
CND_ARTIFACT_DIR_GNU=dist/GNU/GNU-Solaris-x86
CND_ARTIFACT_NAME_GNU=dlight_simple_tests
CND_ARTIFACT_PATH_GNU=dist/GNU/GNU-Solaris-x86/dlight_simple_tests
CND_PACKAGE_DIR_GNU=dist/GNU/GNU-Solaris-x86/package
CND_PACKAGE_NAME_GNU=dlightsimpletests.tar
CND_PACKAGE_PATH_GNU=dist/GNU/GNU-Solaris-x86/package/dlightsimpletests.tar
# SUN configuration
CND_PLATFORM_SUN=
CND_ARTIFACT_DIR_SUN=dist/SUN/
CND_ARTIFACT_NAME_SUN=dlight_simple_tests
CND_ARTIFACT_PATH_SUN=dist/SUN//dlight_simple_tests
CND_PACKAGE_DIR_SUN=dist/SUN//package
CND_PACKAGE_NAME_SUN=dlightsimpletests.tar
CND_PACKAGE_PATH_SUN=dist/SUN//package/dlightsimpletests.tar
# MAC configuration
CND_PLATFORM_MAC=GNU-MacOSX
CND_ARTIFACT_DIR_MAC=dist/MAC/GNU-MacOSX
CND_ARTIFACT_NAME_MAC=dlight_simple_tests
CND_ARTIFACT_PATH_MAC=dist/MAC/GNU-MacOSX/dlight_simple_tests
CND_PACKAGE_DIR_MAC=dist/MAC/GNU-MacOSX/package
CND_PACKAGE_NAME_MAC=dlightsimpletests.tar
CND_PACKAGE_PATH_MAC=dist/MAC/GNU-MacOSX/package/dlightsimpletests.tar
# SUN-x86-64 configuration
CND_PLATFORM_SUN-x86-64=
CND_ARTIFACT_DIR_SUN-x86-64=dist/SUN-x86-64/
CND_ARTIFACT_NAME_SUN-x86-64=dlight_simple_tests
CND_ARTIFACT_PATH_SUN-x86-64=dist/SUN-x86-64//dlight_simple_tests
CND_PACKAGE_DIR_SUN-x86-64=dist/SUN-x86-64//package
CND_PACKAGE_NAME_SUN-x86-64=dlightsimpletests.tar
CND_PACKAGE_PATH_SUN-x86-64=dist/SUN-x86-64//package/dlightsimpletests.tar
# GNU-64 configuration
CND_PLATFORM_GNU-64=GNU-Solaris-x86
CND_ARTIFACT_DIR_GNU-64=dist/GNU-64/GNU-Solaris-x86
CND_ARTIFACT_NAME_GNU-64=dlight_simple_tests
CND_ARTIFACT_PATH_GNU-64=dist/GNU-64/GNU-Solaris-x86/dlight_simple_tests
CND_PACKAGE_DIR_GNU-64=dist/GNU-64/GNU-Solaris-x86/package
CND_PACKAGE_NAME_GNU-64=dlightsimpletests.tar
CND_PACKAGE_PATH_GNU-64=dist/GNU-64/GNU-Solaris-x86/package/dlightsimpletests.tar
# GNU-Linux configuration
CND_PLATFORM_GNU-Linux=GNU-Linux-x86
CND_ARTIFACT_DIR_GNU-Linux=dist/GNU-Linux/GNU-Linux-x86
CND_ARTIFACT_NAME_GNU-Linux=dlight_simple_tests
CND_ARTIFACT_PATH_GNU-Linux=dist/GNU-Linux/GNU-Linux-x86/dlight_simple_tests
CND_PACKAGE_DIR_GNU-Linux=dist/GNU-Linux/GNU-Linux-x86/package
CND_PACKAGE_NAME_GNU-Linux=dlightsimpletests.tar
CND_PACKAGE_PATH_GNU-Linux=dist/GNU-Linux/GNU-Linux-x86/package/dlightsimpletests.tar
