<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE helpset PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 2.0//EN"
                         "http://java.sun.com/products/javahelp/helpset_2_0.dtd">
<!--       
*     Copyright © 1997, 2011, Oracle and/or its affiliates. All rights reserved.
*     Use is subject to license terms.
-->
<helpset version="2.0">
	<title>Help for NetBeans IDE for PHP</title>
	<maps>
		<homeID>org.netbeans.modules.php.help.about</homeID>
		<mapref location="help-map.xml"/>
	</maps>
	<view mergetype="javax.help.AppendMerge">
		<name>TOC</name>
		<label>Table of Contents</label>
		<type>javax.help.TOCView</type>
		<data>help-toc.xml</data>
	</view>

	<view>
		<name>Search</name>
		<label>Search</label>
		<type>javax.help.SearchView</type>
		<data engine="com.sun.java.help.search.DefaultSearchEngine">JavaHelpSearch</data>
	</view>
</helpset>
