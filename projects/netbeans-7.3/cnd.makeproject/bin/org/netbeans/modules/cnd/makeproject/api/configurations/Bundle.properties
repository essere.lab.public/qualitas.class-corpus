# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
#
# Copyright 1997-2010 Oracle and/or its affiliates. All rights reserved.
#
# Oracle and Java are registered trademarks of Oracle and/or its affiliates.
# Other names may be trademarks of their respective owners.
#
# The contents of this file are subject to the terms of either the GNU
# General Public License Version 2 only ("GPL") or the Common
# Development and Distribution License("CDDL") (collectively, the
# "License"). You may not use this file except in compliance with the
# License. You can obtain a copy of the License at
# http://www.netbeans.org/cddl-gplv2.html
# or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
# specific language governing permissions and limitations under the
# License.  When distributing the software, include this License Header
# Notice in each file and include the License file at
# nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
# particular file as subject to the "Classpath" exception as provided
# by Oracle in the GPL Version 2 section of the License file that
# accompanied this code. If applicable, add the following below the
# License Header, with the fields enclosed by brackets [] replaced by
# your own identifying information:
# "Portions Copyrighted [year] [name of copyright owner]"
#
# Contributor(s):
#
# The Original Software is NetBeans. The Initial Developer of the Original
# Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
# Microsystems, Inc. All Rights Reserved.
#
# If you wish your version of this file to be governed by only the CDDL
# or only the GPL Version 2, indicate your decision by adding
# "[Contributor] elects to include this software in this distribution
# under the [CDDL or GPL Version 2] license." If you do not indicate a
# single choice of license, a recipient has the option to distribute
# your version of this file under either the CDDL, the GPL Version 2 or
# to extend the choice of license to its licensees as provided above.
# However, if you add GPL Version 2 code and therefore, elected the GPL
# Version 2 license, then the option applies only if the new code is
# made subject to such option by the copyright holder.

# MakefileConfiguration
WorkingDirectory_LBL=Working Directory
BuildCommandLine_LBL=Build Command
CleanCommandLine_LBL=Clean Command
BuildResult_LBL=Build Result
WorkingDirectory_TT=The directory in which the build and clean commands are executed.
BuildCommandLine_TT=The command that builds the project.
CleanCommandLine_TT=The command that cleans the project.
BuildResult_TT=The path to the output of the Build Command and could be a path to an executable or library.
ProjectsTxt1=Related Projects
ProjectsHint=Projects
ProjectsTxt2=Projects:

#Common
GeneralTxt=General
GeneralHint=General
AdditionalDependenciesTxt1=Additional Dependencies
AdditionalDependenciesHint=Additional dependencies
AdditionalDependenciesTxt2=Additional dependencies:
InputTxt=Input
InputHint=Input
OutputTxt=Output
OutputHint=Name of output file or output directory.
OtherOptionsTxt=Other Options
OtherOptionsHint=Other options
ToolTxt1=Tool
ToolHint1=The name of the tool to use for this operation.
ToolTxt2=Tool
ToolHint2=Tool
RanlibToolTxt=Indexer tool
RanlibToolHint=Name of indexer tool
OptionsTxt=Options
OptionsHint=Options
StripSymbolsTxt=Strip Symbols
StripSymbolsHint=Strip symbols
AdditionalOptionsTxt1=Additional Options
AdditionalOptionsHint=Additional options
AdditionalOptionsTxt2=Additional options:
AllOptionsTxt=All options:
CommandLineTxt=Compilation Line
CommandLineHint=Compilation line was used to compile this compilation unit
CompileFolderTxt=Compilation Folder
CompileFolderHint=Folder where compilation unit was compiled

SPECIAL_CHARATERS_ERROR=Special characters including spaces are not allowed in Output

# Archiver Configuration
RunRanlibTxt=Run Ranlib
RunRanlibHint=Whether to run ranlib
VerboseTxt=Verbose
SupressDiagnosticsTxt=Suppress Diagnostics
SupressDiagnosticsHint=Suppress diagnostics messages
InheritedValuesTxt=Inherited values:

FilesTxt=Files

#Code Assistance Configuration
CodeAssistanceTxt=Code Assistance
CodeAssistanceHint=Configuring code assistance
BuildAnalyzerTxt=Use Build Analyzer
BuildAnalyzerHint=Configure code assistance by build artifacts after each building. 
ToolsTxt2=List of Analyzed Tools
ToolsHint2=Column separated compiler names. Analyzer uses list to collect compiler invocations at project building time.
TransientMacrosTxt=Transient Macros
TransientMacrosHint=List of transient macros (macros that depend on time, date or specific environment). Value of transient macros is not stored in the public project metadata.
TransientMacrosLbl=&Transient Macros:
EnvironmentVariablesTxt=User Environment Variables
EnvironmentVariablesHint=List of environment variables that are used in include strings to replace system specific paths.
EnvironmentVariablesLbl=User Environment &Variables

# Basic Compiler Configuration
FastBuildTxt=No Flags
DebugTxt=Debug
PerformanceDebugTxt=Performance Debug
TestCoverageTxt=Test Coverage
DiagnosableReleaseTxt=Diagnosable Release
ReleaseTxt=Release
PerformanceReleaseTxt=Performance Release
NoWarningsTxt=No Warnings
SomeWarningsTxt=Some Warnings
MoreWarningsTxt=More Warnings
ConvertWarningsTxt=Convert Warnings to Errors
BasicOptionsTxt=Basic Options
BasicOptionsHint=Basic options
DevelopmentModeTxt=Development Mode
DevelopmentModeHint=Development mode
WarningLevelTxt=Warning Level
WarningLevelHint=Warning level
64BitArchitectureTxt=Architecture
64BitArchitectureHint=32 or 64 bit architecture
BITS_DEFAULT=<Default>
BITS_32=32 Bits
BITS_64=64 Bits
CPPStandardTxt=C++ Standard
CPPStandardHint=C++11 or C++98 C++ standard
STANDARD_DEFAULT=<Default>
STANDARD_INHERITED=<Inherited>
STANDARD_INHERITED_WITH_VALUE=<Inherited> ({0})
STANDARD_CPP98=C++98
STANDARD_CPP11=C++11
CStandardTxt=C Standard
CStandardHint=C89 or C99 C standard
STANDARD_C89=C89
STANDARD_C99=C99

# CCCCompilerConfiguration
NoneTxt=None
SafeTxt=Safe
AutomaticTxt=Automatic
OpenMPTxt=OpenMP
RuntimeOnlyTxt=Runtime Only
ClassicIostreamsTxt=Classic Io Streams
BinaryStandardTxt=Binary Standard
ConformingStandardTxt=Conforming Standard
OldTxt=Old
LegacyTxt=Legacy
DefaultTxt=Default
ModernTxt=Modern
AllTxt=All
IncludeDirectoriesTxt=Include Directories
IncludeDirectoriesHint=Include directories
PreprocessorDefinitionsTxt=Preprocessor Definitions
PreprocessorDefinitionsHint=Preprocessor definitions
PreprocessorDefinitionsLbl=Prepr&ocessor Definitions:
PreprocessorUndefinedTxt=Preprocessor Undefined
PreprocessorUndefinedHint=Preprocessor undefined macros. Macro names are prefixed with the compiler option (usually -U).
PreprocessorUndefinedLbl=Preprocessor &Undefined:
UseLinkerLibrariesTxt=Use Linker Libraries
UseLinkerLibrariesHint=Use pkg-config libraries defined in the Linker configuration.\
 Checked property adds user include paths and user defined macros for each pkg-config library by invoking pkg-config by command:<br>\
 `pkg-config --cflags libraryName`

# CCCompilerConfiguration
MultithreadingLevelTxt=Multithreading Level
MultithreadingLevelHint=Note for OpenMP on Solaris and Linux: unless already defined, the IDE will set the maximum number of threads the program can create to 2 via the environment variable OMP_NUM_THREADS. \
Go to the Run|Environment property in the project properties and set OMP_NUM_THREADS to the number of your choice if you want to change this.
LibraryLevelTxt=Library Level
LibraryLevelHint=Library level
StandardsEvolutionTxt=Standards Evolution
StandardsEvolutionHint=Standards evolution
LanguageExtensionsTxt=Language Extensions
LanguageExtensionsHint=Language extensions

# Configuration
ActiveTxt=(active)

# CustomToolConfiguration
PerformingStepTxt=Performing Custom Build Step
CustomBuildTxt=Custom Build
CustomBuildHint=Custom build
CommandLineTxt2=Command Line
CommandLineHint2=Command line
DescriptionTxt=Description
DescriptionHint=Description
OutputsTxt=Outputs
OutputsNint=Outputs

# Folder
NewFolderName=New Folder
NewTestFolderName=New Test

# Item Configuration
ItemTxt=Item
ItemHint=Item
NameTxt=Name
FilePathTxt=File Path
FullFilePathTxt=Full File Path
LastModifiedTxt=Last Modified
ItemConfigurationTxt=Item Configuration
ItemConfigurationHint=Item configuration
ExcludedFromBuildTxt=Excluded From Build
ExcludedFromBuildHint=Excluded from build
ExcludedFromCodeAssistanceTxt=Exclude From Code Assistance
ExcludedFromCodeAssistanceHint=Exclude from code assistance

# Folder Configuration
FolderConfiguration=Folder Configuration
FolderConfigurationTxt=Folder Configuration
FolderConfigurationHint=Folder Configuration

# LibraryItem
ProjectTxt=Project: {0}
StandardLibraryTxt=Standard Library: {0} ({1})
LibraryTxt=Library: {0}
LibraryFileTxt=Library File: {0}
LibraryOptionTxt=Library Option: {0}

# Linker Configuration
PositionIndependantCodeTxt=Position Independent Code
PositionIndependantCodeHint=Position independent code (-Kpic)
NoRunPathTxt=No Run Path
NoRunPathHint=No run path (-norunpath)Hint
AssignNameTxt=Assign Name
AssignNameHint=Assign name (-h <Output>)
AdditionalLibraryDirectoriesTxt=Additional Library Directories
AdditionalLibraryDirectoriesHint=Additional library directories (-L)
RuntimeSearchDirectoriesTxt=Runtime Search Directories
RuntimeSearchDirectoriesHint=Runtime search directories (-R)
LibrariesTxt1=Libraries
LibrariesHint=Libraries
LibrariesTxt2=Libraries:
AllOptionsTxt2=All Options:
MakefileTxt=Makefile
MakefileHint=Makefile

# MakeConfigurationDescriptor
SourceFilesTxt=Source Files
HeaderFilesTxt=Header Files
ResourceFilesTxt=Resource Files
ImportantFilesTxt=Important Files
TestsFilesTxt=Test Files
AddingFilesTxt=Adding files...
CannotSaveTxt=The project {0} cannot be saved because the following files are write-protected:

# MakeConfiguration
MakefileName=Makefile
ApplicationName=Application
DynamicLibraryName=Dynamic Library
StaticLibraryName=Static Library
DBApplicationName=Database Application
QtApplicationName=Qt Application
QtDynamicLibraryName=Qt Dynamic Library
QtStaticLibraryName=Qt Static Library

ProjectDefaultsTxt=Project Defaults
ProjectDefaultsHint=Project defaults
DevelopmentHostTxt=Build Host
DevelopmentHostHint=Local or remote build host
CompilerCollectionTxt=Tool Collection
CompilerCollectionHint=Tool collection
CRequiredTxt=C Compiler Required
CRequiredHint=The project requires a C compiler to build
CppRequiredTxt=C++ Compiler Required
CppRequiredHint=The project requires a C++ compiler to build
FortranRequiredTxt=Fortran Compiler Required
FortranRequiredHint=The project requires a Fortran compiler to build
AssemblerRequiredTxt=Assembler Required
AssemblerRequiredHint=The project requires an assembler to build
PlatformTxt=Target Platform
PlatformHint=Target Platform is the platform (Solaris, Linux, Windows, Mac, ...) the project is built for.
ConfigurationTypeTxt=Configuration Type
ConfigurationTypeHint=Configuration type
ProjectLocationTxt=Project Folder
DependencyCheckingTxt=Enable Make Dependency Checking
DependencyCheckingHint=Enable dependency checking in generated makefiles (requires support from tool collection).
RebuildPropChangedTxt=Rebuild if properties have changed
RebuildPropChangedHint=Rebuild entire project if project properties have changed

# CompilerSet2Configuration
NOT_FOUND={0} - not found
NOT_CONFIGURED={0} - Not configured

# PackagingConfiguration
PackageTypeName=Package Type
PackageTypeHint=The type of this package.
FilesName=Packaging Files
FilesHint=Files and other information in this package.
VerboseName=Verbose Build
VerboseHint=Turns verbose build output on or off.
DefaultArchictureValue=Architecture...

# QtConfiguration
QtGeneralTxt=General
QtGeneralHint=General Qmake Configuration
QtDestdirTxt=Destination Directory
QtDestdirHint=Directory where build artifact is be placed
QtTargetTxt=Target Name
QtTargetHint=Target name without "lib" prefix and suffixes
QtVersionTxt=Target Version
QtVersionHint=Target application or library version
QtBuildModeTxt=Build Mode
QtBuildModeHint=Build mode
QtModulesTxt=Qt Modules
QtModulesHint=Qt modules used by the project
QtCoreTxt=QtCore
QtCoreHint=QtCore module (should be enabled in most cases)
QtGuiTxt=QtGui
QtGuiHint=QtGui module (should be enabled in most cases)
QtNetworkTxt=QtNetwork
QtNetworkHint=QtNetwork module
QtOpenglTxt=QtOpenGL
QtOpenglHint=QtOpenGL module
QtPhononTxt=Phonon
QtPhononHint=Phonon Multimedia Framework (since Qt 4.4)
Qt3SupportTxt=Qt3Support
Qt3SupportHint=Qt3Support module
QtSqlTxt=QtSql
QtSqlHint=QtSql module
QtSvgTxt=QtSvg
QtSvgHint=QtSvg module
QtXmlTxt=QtXml
QtXmlHint=QtXml module
QtWebkitTxt=QtWebKit
QtWebkitHint=WebKit integration (since Qt 4.4)
QtIntermediateFilesTxt=Intermediate Files
QtIntermediateFilesHint=Intermediate Files
QtMocDirTxt=MOC Directory
QtMocDirHint=Directory where generated moc_* files will be placed
QtRccDirTxt=RCC Directory
QtRccDirHint=Directory where generated qrc_* files will be placed
QtUiDirTxt=UI Directory
QtUiDirHint=Directory where generated ui_* files will be placed
QtExpertTxt=Expert
QtExpertHint=Expert settings
QtQmakeSpecTxt=Qmake Spec
QtQmakeSpecHint=Value of -spec option passed to qmake. Leave empty for default value.
QtCustomDefsTxt=Custom Definitions
QtCustomDefsHint=Custom qmake definitions
QtCustomDefsLbl=Custom &qmake Definitions:

#Upgrade Dialog
UPGRADE_TXT=This will upgrade the project to the current project version.\n\n\
Would you like to continue?\n\n\
If you choose Yes, the project will be upgraded to the current version and the project can no longer be opened with the earlier version of the IDE.
UPGRADE_DIALOG_TITLE=Upgrade Project
UPGRADE_TXT_AUTO=Yes

MakeConfigurationDescriptor.SaveConfigurationTitle=Saving Project
MakeConfigurationDescriptor.SaveConfigurationText=Please wait until the project metadata is saved...

