<?xml version='1.0' encoding='ISO-8859-1'?>
<!DOCTYPE toc PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp TOC Version 2.0//EN"
                     "http://java.sun.com/products/javahelp/toc_2_0.dtd">

<toc version="2.0">
    <tocitem text="Web Applications" expand="false">
        <tocitem text="About Web Applications">
            <tocitem text="About Web Applications" target="about_webapps" />
            <tocitem text="Working with Web Applications" target="webapps_process" />
            <tocitem text="Web Application Tasks: Quick Reference" target="webapps_quickref" />
            <tocitem text="About the IDE's Web Development Support" target="webapps_idesupport" />
            <tocitem text="About the IDE's Technology Version Support" target="webapps_techsupport" />
        </tocitem>
        <tocitem text="Creating Web Applications">
            <tocitem text="About Creating Web Applications" target="about_create_webapps" />
            <tocitem text="Creating a Web Application Project" target="create_webapps_proj" />
        </tocitem>
        <tocitem text="Creating and Editing Web Components">
            <tocitem text="About Creating Web Components" target="about_create_webcomps" />
            <tocitem text="JSP and HTML Files">
                <tocitem text="About Creating and Editing JSP, HTML, and Tag Files" target="about_jsp_edit" />
                <tocitem text="About JavaServer Pages Technology" target="about_jsp_technology" />
                <tocitem text="About JSP Syntax" target="about_jsp_syntax" />
                <tocitem text="Creating a JSP File" target="jsp_create" />
                <tocitem text="Creating an HTML File" target="html_create" />
                <tocitem text="Editing a JSP, HTML, or Tag File" target="jsp_edit" />
                <tocitem text="Setting Character Encoding for a JSP File">
                    <tocitem text="About JSP Character Encoding" target="about_jsp_encoding" />
                    <tocitem text="Setting Request Character Encoding for a JSP File" target="jsp_encoding_request" />
                    <tocitem text="Setting Page Character Encoding for a JSP File" target="jsp_encoding_page" />
                    <tocitem text="Setting Response Character Encoding for a JSP File" target="jsp_encoding_response" />
                </tocitem>
                <tocitem text="Accessing a Custom Tag from a JSP Page" target="jsp_usetaglib" />
                <tocitem text="Accessing an Applet from a JSP Page" target="jsp_useapplet" />
                <tocitem text="Compiling a JSP File" target="jsp_compile" />
                <tocitem text="Viewing a JSP File's Servlet" target="jsp_servletview" />
                <tocitem text="Passing Request Parameters" target="jsp_queryparams" />
                <tocitem text="Running a JSP File" target="jsp_run" />
            </tocitem>
            <tocitem text="Tags">
                <tocitem text="About Tag Libraries" target="about_taglib" />
                <tocitem text="About the JSTL" target="about_jstl" />
                <tocitem text="Tag Library Descriptors">
                    <tocitem text="About Tag Library Descriptors" target="about_tld" />
                    <tocitem text="Creating a Tag Library Descriptor" target="tld_create" />
                    <tocitem text="Editing a Tag Library Descriptor" target="tld_edit" />
                </tocitem>
                <tocitem text="Tag Files (JSP syntax)">
                    <tocitem text="About Tag Files" target="about_tagfile" />
                    <tocitem text="Creating a Tag File" target="tagfile_create" />
                    <tocitem text="Editing a JSP, HTML, or Tag File" target="jsp_edit" />
                </tocitem>
                <tocitem text="Tag Handlers (Java programming language)">
                    <tocitem text="Creating a Tag Handler" target="taghandler_create" />
                    <tocitem text="Defining a Tag Handler's TLD Information" target="taghandler_edit" />
                </tocitem>
            </tocitem>
            <tocitem text="Applets">
                <tocitem text="Creating an Applet" target="applet_create" />
                <tocitem text="Running an Applet" target="applet_run" />
                <tocitem text="Setting Permissions for an Applet" target="applet_security" />
            </tocitem>
            <tocitem text="Servlets">
                <tocitem text="About Servlets" target="about_servlet" />
                <tocitem text="Creating a Servlet" target="servlet_create" />
                <tocitem text="Editing a Servlet" target="servlet_edit" />
                <tocitem text="Viewing a JSP File's Servlet" target="jsp_servletview" />
                <tocitem text="Passing Request Parameters" target="jsp_queryparams" />
                <tocitem text="Running a Servlet" target="servlet_run" />
            </tocitem>
            <tocitem text="Filters">
                <tocitem text="About Filters" target="about_filter" />
                <tocitem text="Creating a Filter" target="filter_create" />
                <tocitem text="Registering a Filter" target="filter_addtodd" />
                <tocitem text="Running a Filter" target="filter_run" />
            </tocitem>
            <tocitem text="Web Application Listeners">
                <tocitem text="About Web Application Listeners" target="about_listener" />
                <tocitem text="Creating a Web Application Listener" target="listener_create" />
                <tocitem text="Registering a Web Application Listener" target="listener_addtodd" />
                <tocitem text="Running a Web Application Listener" target="listener_run" />
            </tocitem>
        </tocitem>
        <tocitem text="Configuring Web Applications">
            <tocitem text="About Configuring Web Applications" target="about_webapp_configure" />
            <tocitem text="Configuring Web Application Deployment Descriptors" target="configure_webapp_dd" />
        </tocitem>
        <!-- <tocitem text="Building Web Applications">
            <tocitem text="About Building Web Applications" target="about_build" />
            <tocitem text="Packaging a Resource in a WAR File" target="build_externalresources" />
            <tocitem text="Building a WAR File" target="build_warfile" />
            <tocitem text="Removing a WAR File" target="build_remove" />
        </tocitem> -->
        <tocitem text="Deploying Web Applications">
            <tocitem text="About Deploying Web Applications" target="about_deploy" />
            <tocitem text="Web Application Deployment Tasks: Quick Reference" target="deploy_quickref" />
            <tocitem text="Passing Request Parameters" target="jsp_queryparams" />
            <tocitem text="Deploying a Web Application" target="deploy_webapp" />
            <tocitem text="Changing the Target Server" target="deploy_changeserver" />
        </tocitem>
        <tocitem text="Web Applications on the Cloud">
            <tocitem text="About Web Applications on the Cloud" target="about-cloud"/>
            <tocitem text="Working With Web Applications on the Cloud" target="cloud-process" />
            <tocitem text="Registering a Cloud Account" target="register-cloud"/>
            <tocitem text="Developing Cloud Applications Locally" target="develop-locally"/>
            <tocitem text="Deploying Web Applications to the Cloud" target="deploy-cloud"/>
            <!--<tocitem text="Whitelisting in Oracle Cloud" target="oracle-cloud-whitelisting"/>-->
        </tocitem>
        <tocitem text="Debugging and Testing Web Applications">
            <tocitem text="About Debugging and Testing Web Applications" target="about_debug" />
            <tocitem text="Compiling a JSP File" target="jsp_compile" />
            <tocitem text="Viewing a JSP File's Servlet" target="jsp_servletview" />
            <tocitem text="Debugging a Web Application" target="debug_webapp" />
            <tocitem text="Profiling a Web Application" target="about_profilewebapp" />
            <tocitem text="Creating a Debug Target for a Free-Form Web Project" target="proj_webfree_debug" />
        </tocitem>
        <tocitem text="Web Application Frameworks">
            <tocitem text="About Web Application Frameworks" target="about_framework" />
            <tocitem text="JavaServer Faces">
                <tocitem text="About JavaServer Faces Framework Support" target="framework_jsf_about" />
                <tocitem text="Creating a New Application with JSF Support" target="framework_jsf_create" />
                <tocitem text="Adding JSF Support to an Existing Application" target="framework_jsf_add" />
                <tocitem text="Creating JSF Pages" target="framework_jsf_create_page" />
                <tocitem text="Editing JSF Pages" target="framework_jsf_edit" />
                <tocitem text="Creating Facelets Templates" target="framework_jsf_facelets_template" />
                <tocitem text="Creating Composite Components" target="framework_jsf_composite" />
                <tocitem text="Creating JSF Managed Beans" target="framework_jsf_create_managed_bean" />
                <tocitem text="Working with JSF Components in the Palette" target="framework_jsf_palette" />
                <tocitem text="Creating a JSF Form for Entity Data" target="framework_jsf_form_entity" />
                <tocitem text="Creating a JSF Data Table for Entity Data" target="framework_jsf_datatable_entity" />
                <tocitem text="About JSF CRUD Applications" target="framework_jsf_crud" />
                <tocitem text="Generating JSF Pages from Entity Classes" target="framework_jsf_generate_pages" />
            </tocitem>
            <tocitem text="Grails">
                <tocitem text="About Grails Framework Support" target="framework_grails_about" />
                <tocitem text="Creating a New Web Application with Grails Framework Support" target="framework_grails_create" />
                <tocitem text="Installing a Grails Plugin into a Grails Application" target="framework_grails_install" />
            </tocitem>
            <tocitem text="Spring">
                <tocitem text="About Spring Web MVC Framework Support" target="framework_spring_about" />
                <tocitem text="Creating a New Web Application with Spring MVC Framework Support" target="framework_spring_create" />
                <tocitem text="Adding Spring Web MVC Framework Support to an Existing Application" target="framework_spring_add" />
            </tocitem>
            <tocitem text="Struts">
                <tocitem text="About Struts Framework Support" target="framework_struts_about" />
                <tocitem text="Creating a New Application with Struts Support" target="framework_struts_create" />
                <tocitem text="Adding Struts Support to an Existing Application" target="framework_struts_add" />
            </tocitem>
            <tocitem text="Hibernate">
                <tocitem text="Creating a New Web Application with Hibernate Support" target="framework_hibernate_create" />
                <tocitem text="Adding Hibernate Support to a Project" target="framework_hibernate_addsupport" />
            </tocitem>
        </tocitem>
    </tocitem>
</toc>
