/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */


package org.netbeans.modules.websvc.manager.ui;

import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.HelpCtx;
import javax.swing.text.html.HTMLEditorKit;

import javax.swing.JButton;

import java.awt.Dialog;
import java.lang.reflect.InvocationTargetException;

/**
 * This Dialog will show exceptions encountered while a user is testing a web service client method.
 * @author  David Botterill
 */
public class MethodExceptionDialog extends javax.swing.JPanel {
    
    private String currentMessage = "";
    private JButton okButton = new JButton(NbBundle.getMessage(this.getClass(), "OPTION_OK"));
    private DialogDescriptor dlg;
    private Dialog dialog;
    
    /** Creates new form MethodExceptionDialog */
    public MethodExceptionDialog(Throwable inException) {
        initComponents();
        messagePane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(this.getClass(), 
                "MethodExceptionDialog.messagePane.ACC_name"));
        messagePane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(this.getClass(), 
                "MethodExceptionDialog.messagePane.ACC_desc"));
        
        // Only show the root cause of the exception if there was an exception during the method call
        if (inException instanceof InvocationTargetException) {
            inException = ((InvocationTargetException)inException).getTargetException();
        }
        
        setMessage(inException,false);
        
    }
    public void show(){
        
        dlg = new DialogDescriptor(this, NbBundle.getMessage(this.getClass(), "CLIENT_EXCEPTION"),
        false, NotifyDescriptor.OK_CANCEL_OPTION, DialogDescriptor.OK_OPTION,
        DialogDescriptor.DEFAULT_ALIGN, this.getHelpCtx(), null);
        dlg.setOptions(new Object[] { okButton });
        dialog = DialogDisplayer.getDefault().createDialog(dlg);
        dialog.setPreferredSize(new java.awt.Dimension(500,300));
        dialog.show();
    }
    public HelpCtx getHelpCtx() {
        return new HelpCtx("projrave_ui_elements_server_nav_test_websvcdb");
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        scrollPane = new javax.swing.JScrollPane();
        messagePane = new javax.swing.JEditorPane();

        setLayout(new java.awt.BorderLayout());

        messagePane.setEditorKit(new HTMLEditorKit()
        );
        scrollPane.setViewportView(messagePane);

        add(scrollPane, java.awt.BorderLayout.CENTER);

    }//GEN-END:initComponents
    
    public void setMessage(Throwable inException,boolean keepOld){
        
        
        String htmlStart = "<HTML><HEAD>" +
        "<style type=\"text/css\">" +
        "body { font-family: Verdana, sans-serif; font-size: 12; }" +
        "</style>" +
        "</HEAD>" +
        "<BODY>";
        String htmlEnd = "</BODY></HTML>";
        /**
         * unwrap the exceptions.
         */
        Throwable cause = inException;
        StringBuilder builder = new StringBuilder();
        while(null != cause) {
            builder.append( "<BR><FONT COLOR=\"RED\">" );
            builder.append( cause.getLocalizedMessage());
            builder.append(  "</FONT>");
            StackTraceElement [] traceElements = cause.getStackTrace();
            builder.append( "<BR>Stack Trace<BR>");
            for(int ii=0;ii < traceElements.length;ii++) {
                builder.append( "<BR>" );
                builder.append(  traceElements[ii].toString());
            }
            cause = cause.getCause();
        }
        
        
        if(keepOld) {
            builder.insert(0, currentMessage);
        } 
        builder.insert(0, htmlStart);
        builder.append(htmlEnd);
        messagePane.setText(builder.toString());
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane messagePane;
    private javax.swing.JScrollPane scrollPane;
    // End of variables declaration//GEN-END:variables
    
}
